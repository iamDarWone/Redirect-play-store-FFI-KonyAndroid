package com.market.ds;

import java.util.List;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.net.Uri;

import com.konylabs.android.KonyMain;

public class redireccionarActivity {
	 static String paquete ;
	

	 
	   public static void redireccionaMarket() {
			Context context = KonyMain.getActivityContext();
		   String paquete = context.getPackageName();
		   
			Intent rateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + paquete));
		    boolean marketFound = false;

		    // find all applications able to handle our rateIntent
		    final List<ResolveInfo> otherApps = context.getPackageManager().queryIntentActivities(rateIntent, 0);
		    for (ResolveInfo otherApp: otherApps) {
		        // look for Google Play application
		        if (otherApp.activityInfo.applicationInfo.packageName.equals("com.android.vending")) {

		            ActivityInfo otherAppActivity = otherApp.activityInfo;
		            ComponentName componentName = new ComponentName(
		                    otherAppActivity.applicationInfo.packageName,
		                    otherAppActivity.name
		                    );
		            rateIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
		            rateIntent.setComponent(componentName);
		            context.startActivity(rateIntent);
		            marketFound = true;
		            break;

		        }
		    }

		    // if GP not present on device, open web browser
		    if (!marketFound) {
		        Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id="+paquete));
		        context.startActivity(webIntent);
		    }
		}
	   
	   
	   public static void valorar(){
		   Context context = KonyMain.getActivityContext();
		   AlertDialog.Builder builder = new AlertDialog.Builder(context);
		   builder.setMessage("Valora Compensar en Google Play!")
		          .setCancelable(false)
		          .setPositiveButton("Ir a Google Play", new DialogInterface.OnClickListener() {
		              public void onClick(DialogInterface dialog, int id) {
		                   dialog.cancel();
		                   //context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+paquete) ) );     
		                   redireccionaMarket();
		              }
		          })
		          .setNegativeButton("Ahora no", new DialogInterface.OnClickListener() {
		              public void onClick(DialogInterface dialog, int id) {
		   	       		dialog.cancel();
		              }
		          });
		   AlertDialog alert = builder.create();
		   alert.show();
			
	    }
	   
	   public static void redireccionarWEB(){
		  
		   Context context = KonyMain.getActivityContext();
		   paquete = context.getPackageName();
		   Intent intent = new Intent(Intent.ACTION_VIEW);
		   intent.setData(Uri.parse("market://details?id="+paquete));
		   context.startActivity(intent);
	    }
	
		

}
